package testbed;

import FORTH.WordCompiler;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FORTHTestbed
{
    public static void main(String[] _args)
    {
        String fileName = ".\\test.FORTH";
        boolean verbose = false;


        if(_args.length > 0)
        {
            fileName = _args[0];
        }

        if(_args.length > 1)
        {

            if(_args[1].equals("-v"))
            {
                verbose = true;
            }
        }

        StringBuilder builder = new StringBuilder();

        try
        {
            FileReader fileReader = new FileReader(fileName);

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;

            int i = 1;

            while ((line = bufferedReader.readLine()) != null)
            {
                builder.append(line).append("\n");
                i++;
            }

            bufferedReader.close();
        } catch (FileNotFoundException ex)
        {
            throw new RuntimeException("Unable to open file " + fileName + ".");
        } catch (IOException ex)
        {
            throw new RuntimeException("Error reading file " + fileName + ".");
        }

        String source = builder.toString();

        //System.out.println(source);

        /*
        WordCompiler compiler = new WordCompiler();

        //compiler.diagramLexedTokens(compiler.lexSource(source));

        compiler.parseSource(compiler.lexSource(source));
        //*/

        WordCompiler.compileAndRun(source,(String _log) -> System.out.println(_log), true);
    }
}
