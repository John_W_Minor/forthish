package FORTH;

public interface IReturnCode
{
    String name();

    long value();

    Register getRegister();

    enum DefaultCodes implements IReturnCode
    {
        PC_FORWARD_1,
        RESET,
        TERMINATE;

        @Override
        public long value()
        {
            return 0;
        }

        @Override
        public Register getRegister()
        {
            return null;
        }
    }

    enum Register
    {
        EAX_REG,
        EBX_REG,
        ECX_REG,
        EDX_REG
    }
}
