package FORTH;

import java.util.List;
import java.util.Stack;

class CompoundWord implements IWord
{
    private final String name;
    private final List<IWord> definition;

    CompoundWord(String _name, List<IWord> _definition)
    {
        name = _name;
        definition = _definition;
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public IReturnCode process(Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx)
    {
        _runtime.incrementDepth();

        for (int i = 0; i < definition.size(); )
        {
            IReturnCode returnCode = _runtime.processWord(definition.get(i), _stack, _eax, _ebx, _ecx, _edx);

            if (returnCode == IReturnCode.DefaultCodes.PC_FORWARD_1)
            {
                i += 1;
            } else if (returnCode == IReturnCode.DefaultCodes.RESET)
            {
                i = 0;
            } else if (returnCode == IReturnCode.DefaultCodes.TERMINATE)
            {
                i = definition.size();
            } else
            {
                IReturnCode.Register register = returnCode.getRegister();

                if(register == IReturnCode.Register.EAX_REG)
                {
                    _eax = returnCode.value();
                } else if (register == IReturnCode.Register.EBX_REG)
                {
                    _ebx = returnCode.value();
                } else if (register == IReturnCode.Register.ECX_REG)
                {
                    _ecx = returnCode.value();
                } else if (register == IReturnCode.Register.EDX_REG)
                {
                    _edx = returnCode.value();
                }
                i += 1;
            }
        }

        _runtime.decrementDepth();

        return IReturnCode.DefaultCodes.PC_FORWARD_1;
    }
}
