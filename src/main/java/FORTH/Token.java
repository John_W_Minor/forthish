package FORTH;

class Token
{
    final TokenType type;
    final String name;
    final int lineNumber;
    Token nextToken;

    public Token(TokenType _type, String _name, int _lineNumber)
    {
        type = _type;
        name = _name;
        lineNumber = _lineNumber;
    }

    boolean hasNextToken()
    {
        return nextToken != null;
    }

    Token extricate()
    {
        return new Token(type, name, lineNumber);
    }

    @Override
    public String toString()
    {
        return "Token type: " + type.name() + " Name: " + name + " Line Number: " + lineNumber;
    }

    enum TokenType
    {
        IF("if"),
        REPEAT("repeat"),
        OPEN_SCOPE("{"),
        CLOSE_SCOPE("}"),
        WORD_DEFINITION(":[^:\\s{}]*"),
        CALLABLE_WORD("[^:\\s{}]+");

        public final String typeSignature;

        TokenType(String _typeSignature)
        {
            typeSignature = _typeSignature;
        }
    }
}
