package FORTH;

import FORTH.logging.FORTHLogger;

import java.util.Stack;

public class WordRuntime
{
    public static final String TAB = "  ";

    private int depth = 0;

    private int processCount = 0;

    private final FORTHLogger logger;

    private boolean verbose = false;

    public WordRuntime(FORTHLogger _logger)
    {
        logger = _logger;
    }

    public WordRuntime(FORTHLogger _logger, boolean _verbose)
    {
        logger = _logger;

        verbose = _verbose;
    }

    void incrementDepth()
    {
        if (verbose)
        {
            logger.log(getIndentation() + "{");
        }

        depth++;
    }

    void decrementDepth()
    {
        depth--;

        if (verbose)
        {
            logger.log(getIndentation() + "}");
        }
    }

    String getIndentation()
    {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < depth; i++)
        {
            builder.append(TAB);
        }

        return builder.toString();
    }

    IReturnCode processWord(IWord _word, Stack<Long> _stack, long _eax, long _ebx, long _ecx, long _edx)
    {
        if (verbose)
        {
            logger.log(getIndentation() + _word.name());
        }

        processCount++;

        return _word.process(_stack, this, _eax, _ebx, _ecx, _edx);
    }

    public void printStatistics()
    {
        logger.log(processCount + " WORDS WERE PROCESSED.");
    }

    public void log(String _log)
    {
        logger.log(_log);
    }
}
