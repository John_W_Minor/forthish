package FORTH;

import java.util.Stack;

interface IWord
{
    String name();

    IReturnCode process(Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx);
}
