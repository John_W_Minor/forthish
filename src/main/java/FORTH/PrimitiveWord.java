package FORTH;

import java.util.Stack;

class PrimitiveWord implements IWord
{
    private final String name;
    private final IPrimitiveFunction function;

    PrimitiveWord(String _name, IPrimitiveFunction _function)
    {
        name = _name;
        function = _function;
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public IReturnCode process(Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx)
    {
        return function.process(_stack, _runtime, _eax, _ebx, _ecx, _edx);
    }
}
