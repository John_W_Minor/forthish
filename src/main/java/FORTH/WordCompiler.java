package FORTH;

import FORTH.logging.FORTHLogger;

import java.math.BigInteger;
import java.util.*;
import java.util.regex.Pattern;

public class WordCompiler
{
    //#region Arithmetic
    static final String PLUS = "+";
    static final String MINUS = "-";
    static final String TIMES = "*";
    static final String DIVIDE = "/";
    static final String POWER = "power";
    //#endregion

    //#region Boolean Expressions
    static final String AND = "&";
    static final String OR = "|";
    static final String XOR = "^";
    static final String NOT = "!";
    static final String GREATER = ">";
    static final String LESSER = "<";
    static final String GREATER_EQUAL = ">=";
    static final String LESSER_EQUAL = "<=";
    static final String EQUAL = "==";
    static final String NOT_EQUAL = "!=";
    static final String TRUE_VAL = "true";
    static final String FALSE_VAL = "false";
    //#endregion

    //#region Stack Manipulators
    static final String DUPLICATE = "duplicate";
    static final String TOP_TO_BOTTOM = "top_to_bottom";
    static final String BOTTOM_TO_TOP = "bottom_to_top";
    static final String SWAP = "swap";
    static final String DROP = "drop";
    static final String SHOW_STACK = "show_stack";
    //#endregion

    //#region register manipulation
    static final String HOLD_EAX = "hold_a";
    static final String SCAN_EAX = "scan_a";
    static final String PLACE_EAX = "place_a";

    static final String HOLD_EBX = "hold_b";
    static final String SCAN_EBX = "scan_b";
    static final String PLACE_EBX = "place_b";

    static final String HOLD_ECX = "hold_c";
    static final String SCAN_ECX = "scan_c";
    static final String PLACE_ECX = "place_c";

    static final String HOLD_EDX = "hold_d";
    static final String SCAN_EDX = "scan_d";
    static final String PLACE_EDX = "place_d";
    //#endregion

    //#region Control Flow
    static final String RESET = "reset";
    static final String CONTINUE = "continue";
    static final String BREAK = "break";
    static final String TERMINATE = "terminate";

    public static WordCompiler generateCompiler(FORTHLogger _logger, boolean _verbose)
    {
        WordCompiler compiler = new WordCompiler();

        //#region Arithmetic
        compiler.build(PLUS,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    _stack.push(b + a);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        /*compiler.build(MINUS,
                (Stack<Long> _stack, WordRuntime _runtime, long _holdValue) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    _stack.push(b - a);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });//*/

        /*compiler.build(TIMES,
                (Stack<Long> _stack, WordRuntime _runtime, long _holdValue) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    _stack.push(b * a);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });//*/

        /*compiler.build(DIVIDE,
                (Stack<Long> _stack, WordRuntime _runtime, long _holdValue) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    _stack.push(b / a);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });//*/

        compiler.build(POWER,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    long c = (long) Math.pow(b, a);
                    _stack.push(c);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });
        //#endregion

        //#region Boolean Expressions
        compiler.build(AND,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    _stack.push(b & a);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(OR,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    _stack.push(b | a);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(XOR,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    _stack.push(b ^ a);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(NOT,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    _stack.push(~a);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(GREATER,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    if (b > a)
                    {
                        _stack.push(-1L);
                    } else
                    {
                        _stack.push(0L);
                    }
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(LESSER,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    if (b < a)
                    {
                        _stack.push(-1L);
                    } else
                    {
                        _stack.push(0L);
                    }
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(GREATER_EQUAL,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    if (b >= a)
                    {
                        _stack.push(-1L);
                    } else
                    {
                        _stack.push(0L);
                    }
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(LESSER_EQUAL,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    if (b <= a)
                    {
                        _stack.push(-1L);
                    } else
                    {
                        _stack.push(0L);
                    }
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(EQUAL,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    if (b == a)
                    {
                        _stack.push(-1L);
                    } else
                    {
                        _stack.push(0L);
                    }
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(NOT_EQUAL,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    if (b != a)
                    {
                        _stack.push(-1L);
                    } else
                    {
                        _stack.push(0L);
                    }
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(TRUE_VAL,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    _stack.push(-1L);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(FALSE_VAL,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    _stack.push(0L);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });
        //#endregion

        //#region Stack Manipulators
        compiler.build(DUPLICATE,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.peek();
                    _stack.push(a);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(TOP_TO_BOTTOM,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    int count = _stack.pop().intValue();

                    long[] elements = new long[count];

                    for (int i = 0; i < count; i++)
                    {
                        elements[i] = _stack.pop();
                    }

                    _stack.push(elements[0]);

                    for (int i = count - 1; i > 0; i--)
                    {
                        _stack.push(elements[i]);
                    }
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(BOTTOM_TO_TOP,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    int count = _stack.pop().intValue();

                    long[] elements = new long[count];

                    for (int i = 0; i < count; i++)
                    {
                        elements[i] = _stack.pop();
                    }

                    for (int i = count - 2; i >= 0; i--)
                    {
                        _stack.push(elements[i]);
                    }

                    _stack.push(elements[count - 1]);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(SWAP,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    long b = _stack.pop();
                    _stack.push(a);
                    _stack.push(b);
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(DROP,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();
                    for (int i = 0; i < a; i++)
                    {
                        _stack.pop();
                    }
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(SHOW_STACK,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    StringBuilder builder = new StringBuilder();

                    for (long i : _stack)
                    {
                        builder.append(i).append(",");
                    }

                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + builder.toString());

                        _runtime.decrementDepth();
                    }

                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });
        //#endregion

        //#region register manipulator
        compiler.build(HOLD_EAX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();

                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + a);

                        _runtime.decrementDepth();
                    }

                    IReturnCode code = new IReturnCode()
                    {
                        @Override
                        public String name()
                        {
                            return "anonymous";
                        }

                        @Override
                        public long value()
                        {
                            return a;
                        }

                        @Override
                        public Register getRegister()
                        {
                            return Register.EAX_REG;
                        }
                    };

                    return code;
                });

        compiler.build(SCAN_EAX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.peek();

                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + a);

                        _runtime.decrementDepth();
                    }

                    IReturnCode code = new IReturnCode()
                    {
                        @Override
                        public String name()
                        {
                            return "anonymous";
                        }

                        @Override
                        public long value()
                        {
                            return a;
                        }

                        @Override
                        public Register getRegister()
                        {
                            return Register.EAX_REG;
                        }
                    };

                    return code;
                });

        compiler.build(PLACE_EAX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + _eax);

                        _runtime.decrementDepth();
                    }

                    _stack.push(_eax);

                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(HOLD_EBX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();

                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + a);

                        _runtime.decrementDepth();
                    }

                    IReturnCode code = new IReturnCode()
                    {
                        @Override
                        public String name()
                        {
                            return "anonymous";
                        }

                        @Override
                        public long value()
                        {
                            return a;
                        }

                        @Override
                        public Register getRegister()
                        {
                            return Register.EBX_REG;
                        }
                    };

                    return code;
                });

        compiler.build(SCAN_EBX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.peek();

                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + a);

                        _runtime.decrementDepth();
                    }

                    IReturnCode code = new IReturnCode()
                    {
                        @Override
                        public String name()
                        {
                            return "anonymous";
                        }

                        @Override
                        public long value()
                        {
                            return a;
                        }

                        @Override
                        public Register getRegister()
                        {
                            return Register.EBX_REG;
                        }
                    };

                    return code;
                });

        compiler.build(PLACE_EBX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + _ebx);

                        _runtime.decrementDepth();
                    }

                    _stack.push(_ebx);

                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(HOLD_ECX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();

                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + a);

                        _runtime.decrementDepth();
                    }

                    IReturnCode code = new IReturnCode()
                    {
                        @Override
                        public String name()
                        {
                            return "anonymous";
                        }

                        @Override
                        public long value()
                        {
                            return a;
                        }

                        @Override
                        public Register getRegister()
                        {
                            return Register.ECX_REG;
                        }
                    };

                    return code;
                });

        compiler.build(SCAN_ECX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.peek();

                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + a);

                        _runtime.decrementDepth();
                    }

                    IReturnCode code = new IReturnCode()
                    {
                        @Override
                        public String name()
                        {
                            return "anonymous";
                        }

                        @Override
                        public long value()
                        {
                            return a;
                        }

                        @Override
                        public Register getRegister()
                        {
                            return Register.ECX_REG;
                        }
                    };

                    return code;
                });

        compiler.build(PLACE_ECX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + _ecx);

                        _runtime.decrementDepth();
                    }

                    _stack.push(_ecx);

                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(HOLD_EDX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();

                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + a);

                        _runtime.decrementDepth();
                    }

                    IReturnCode code = new IReturnCode()
                    {
                        @Override
                        public String name()
                        {
                            return "anonymous";
                        }

                        @Override
                        public long value()
                        {
                            return a;
                        }

                        @Override
                        public Register getRegister()
                        {
                            return Register.EDX_REG;
                        }
                    };

                    return code;
                });

        compiler.build(SCAN_EDX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.peek();

                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + a);

                        _runtime.decrementDepth();
                    }

                    IReturnCode code = new IReturnCode()
                    {
                        @Override
                        public String name()
                        {
                            return "anonymous";
                        }

                        @Override
                        public long value()
                        {
                            return a;
                        }

                        @Override
                        public Register getRegister()
                        {
                            return Register.EDX_REG;
                        }
                    };

                    return code;
                });

        compiler.build(PLACE_EDX,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    if (_verbose)
                    {
                        _runtime.incrementDepth();

                        _logger.log(_runtime.getIndentation() + _edx);

                        _runtime.decrementDepth();
                    }

                    _stack.push(_edx);

                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        //#endregion

        //#region Control Flow
        compiler.build(RESET, (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) -> IReturnCode.DefaultCodes.RESET);

        compiler.build(CONTINUE,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();

                    if (a == 0)
                    {
                        return IReturnCode.DefaultCodes.TERMINATE;
                    }
                    return IReturnCode.DefaultCodes.PC_FORWARD_1;
                });

        compiler.build(BREAK,
                (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                {
                    long a = _stack.pop();

                    if (a == 0)
                    {
                        return IReturnCode.DefaultCodes.PC_FORWARD_1;
                    }
                    return IReturnCode.DefaultCodes.TERMINATE;
                });

        compiler.build(TERMINATE, (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) -> IReturnCode.DefaultCodes.TERMINATE);
        //#endregion

        return compiler;
    }


    private final Map<String, IWord> bank = new HashMap<>();

    //#region word bank
    void put(IWord _word)
    {
        if (bank.get(_word.name()) != null)
        {
            throw new RuntimeException("Word " + _word.name() + " is already defined.");
        }
        bank.put(_word.name(), _word);
    }

    void put(IWord _word, int _lineNumber)
    {
        if (bank.get(_word.name()) != null)
        {
            throw new RuntimeException("Error on Line " + _lineNumber + ", Word " + _word.name() + " is already defined.");
        }
        bank.put(_word.name(), _word);
    }

    IWord get(String _word)
    {
        return bank.get(_word);
    }
    //#endregion

    //#region compiling

    //#region word compiling
    public void build(String _name, IPrimitiveFunction _function)
    {
        if (isIllegalName(_name))
        {
            throw new RuntimeException("Illegal name in Primitive " + _name + ".");
        }

        put(new PrimitiveWord(_name, _function));
    }

    public IWord build(Token _token)
    {
        final String name = _token.name;
        final List<IWord> compiledDefinition = new ArrayList<>();

        if (isIllegalName(name))
        {
            throw new RuntimeException(_token + " is an illegal Word name.");
        }

        //running total of scopes in this word
        int scopes = 0;

        Token previousToken = null;
        Token currentToken = _token;
        Token nextToken = null;

        while (currentToken.hasNextToken())
        {
            //#region tokens

            //the first token is the word name
            previousToken = currentToken;

            currentToken = currentToken.nextToken;

            nextToken = currentToken.nextToken;
            //#endregion

            //#region Numeric Literals
            String cleanToken = currentToken.name.replace("_", "");

            try
            {
                int radix = 10;

                if(cleanToken.length() > 2 && cleanToken.substring(0,2).equals("0b"))
                {
                    radix = 2;
                    cleanToken = cleanToken.substring(2);
                }

                //final long value = Long.parseLong(cleanToken);
                final BigInteger bigValue = new BigInteger(cleanToken, radix);
                final long value = bigValue.longValue();

                final PrimitiveWord pushValue = new PrimitiveWord("push_value_" + value,
                        (Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx) ->
                        {
                            _stack.push(value);

                            return IReturnCode.DefaultCodes.PC_FORWARD_1;
                        });

                compiledDefinition.add(pushValue);

                continue;
            } catch (Exception e)
            {
                //e.printStackTrace();
                //Nothing needs to be here at all because this is basically just a fancy way to switch.
                //I wouldn't have done it this way, except parsing requires the try/catch block.
            }
            //#endregion

            //#region Scope
            if (currentToken.type == Token.TokenType.OPEN_SCOPE)
            {
                //the tokens in the scope will be put into their own anonymous word
                final Token rootAnonymousToken = new Token(Token.TokenType.WORD_DEFINITION, name + SCOPE_SUFFIX + scopes, currentToken.lineNumber);
                Token previousAnonymousToken = null;
                Token currentAnonymousToken = rootAnonymousToken;
                scopes++;

                //if the scope is part of an if statement
                if (previousToken.type == Token.TokenType.IF)
                {
                    previousAnonymousToken = currentAnonymousToken;
                    currentAnonymousToken = new Token(Token.TokenType.CALLABLE_WORD, CONTINUE, currentToken.lineNumber);
                    previousAnonymousToken.nextToken = currentAnonymousToken;
                }

                //we currently know there is one open scope
                int openScopes = 1;

                Token scopeToken = currentToken;

                //read through the rest of the tokens for a matching }
                while (scopeToken.hasNextToken())
                {
                    scopeToken = scopeToken.nextToken;

                    //we need to track all open scopes
                    if (scopeToken.type == Token.TokenType.OPEN_SCOPE)
                    {
                        openScopes++;
                    }

                    //and make sure they all get closed
                    if (scopeToken.type == Token.TokenType.CLOSE_SCOPE)
                    {
                        openScopes--;
                    }

                    //if they are all closed we can stop
                    if (openScopes == 0)
                    {
                        break;
                    }

                    //our goal here is to copy up everything within the scope into an anonymous word
                    previousAnonymousToken = currentAnonymousToken;
                    currentAnonymousToken = scopeToken.extricate();
                    previousAnonymousToken.nextToken = currentAnonymousToken;
                }

                //if the scope is still open after scanning the tokens throw exception
                if (openScopes != 0)
                {
                    throw new RuntimeException(currentToken + " has unclosed Scope.");
                }

                //for the rest of the build process skip the tokens that were put into the anonymous word
                currentToken = scopeToken;

                //if the scope is part of a repeat statement
                if (previousToken.type == Token.TokenType.REPEAT)
                {
                    previousAnonymousToken = currentAnonymousToken;
                    currentAnonymousToken = new Token(Token.TokenType.CALLABLE_WORD, RESET, currentToken.lineNumber);
                    previousAnonymousToken.nextToken = currentAnonymousToken;
                }

                //compiler optimization to avoid compiling empty scope into the program
                if (!rootAnonymousToken.hasNextToken())
                {
                    continue;
                }

                final IWord word = build(rootAnonymousToken);

                compiledDefinition.add(word);

                continue;
            }

            //these are reserved for special compiler work with scopes
            if (currentToken.type == Token.TokenType.IF || currentToken.type == Token.TokenType.REPEAT)
            {
                //and must always be followed by scopen
                if (nextToken == null || nextToken.type != Token.TokenType.OPEN_SCOPE)
                {
                    throw new RuntimeException(currentToken + " without accompanying Scope.");
                }
                continue;
            }
            //#endregion

            //#region Existing Words
            {
                final IWord word = get(currentToken.name);

                if (word == null)
                {
                    throw new RuntimeException(currentToken + " is an unknown Word.");
                }

                compiledDefinition.add(word);
            }
            //#endregion
        }

        IWord word = new CompoundWord(name, compiledDefinition);
        put(word, _token.lineNumber);

        return word;
    }
    //#endregion

    /*
    public Token[] lexSource(String _source)
    {
        if (_source == null || _source.equals(""))
        {
            throw new RuntimeException("No source code detected.");
        }*/

    //String[] wordDefinitions = _source
    //.replaceAll("//.*\n*", " ") //any character after // but \n
    //.replaceAll("/\\*[\\s\\S]*\\*/", " ") //any character at all between /**/
    //.replace("\\*/", " ") // */ gets stripped anyway
    //.replace(":", " ")
    //.replace("{", " { ")
    //.replace("}", " } ")
    //.trim()
    //.split(";");
        /*
        Token[] tokenizedWordDefinitions = new Token[wordDefinitions.length];

        for (int i = 0; i < wordDefinitions.length; i++)
        {
            String[] stringTokens = wordDefinitions[i].trim().split("\\s+");

            String lineNumber = null;

            Token placeHolder = null;

            for (int k = 0; k < stringTokens.length; k++)
            {
                if (stringTokens[k].contains("Ń"))
                {
                    lineNumber = stringTokens[k];
                    continue;
                }

                if (tokenizedWordDefinitions[i] == null)
                {
                    tokenizedWordDefinitions[i] = new Token(stringTokens[k], lineNumber);
                    placeHolder = tokenizedWordDefinitions[i];
                    continue;
                }

                if (placeHolder != null)
                {
                    placeHolder.nextToken = new Token(stringTokens[k], lineNumber);
                    placeHolder = placeHolder.nextToken;
                } else
                {
                    throw new RuntimeException("How did you even reach this code?");
                }
            }
        }

        return tokenizedWordDefinitions;
    }
    */

    public List<Token> lexSource(String _source)
    {
        if (_source == null || _source.equals(""))
        {
            throw new RuntimeException("No source code detected.");
        }

        String[] sourceLines = _source
                .replaceAll("//.*\n?", "\n") //any character after // but \n
                .replace("{", " { ")
                .replace("}", " } ")
                .split("\n");

        List<Token> tokens = new ArrayList<>();

        int lineCount = 1;
        for(String line : sourceLines)
        {
            String[] stringTokens = line.trim().split("\\s");

            for(String stringToken : stringTokens)
            {
                if(stringToken.equals(""))
                {
                    continue;
                }

                if(stringToken.equals(Token.TokenType.IF.typeSignature))
                {
                    tokens.add(new Token(Token.TokenType.IF, "", lineCount));
                } else if(stringToken.equals(Token.TokenType.REPEAT.typeSignature))
                {
                    tokens.add(new Token(Token.TokenType.REPEAT, "", lineCount));
                } else if(stringToken.equals(Token.TokenType.OPEN_SCOPE.typeSignature))
                {
                    tokens.add(new Token(Token.TokenType.OPEN_SCOPE, "", lineCount));
                } else if(stringToken.equals(Token.TokenType.CLOSE_SCOPE.typeSignature))
                {
                    tokens.add(new Token(Token.TokenType.CLOSE_SCOPE, "", lineCount));
                } else if(stringToken.matches(Token.TokenType.WORD_DEFINITION.typeSignature))
                {
                    String nakedWord = stringToken.substring(1);
                    tokens.add(new Token(Token.TokenType.WORD_DEFINITION, nakedWord, lineCount));
                } else if(stringToken.matches(Token.TokenType.CALLABLE_WORD.typeSignature))
                {
                    tokens.add(new Token(Token.TokenType.CALLABLE_WORD, stringToken, lineCount));
                } else
                {
                    throw new RuntimeException("Unknown Token: " + stringToken + " on Line: " + lineCount);
                }
            }

            lineCount++;
        }

        return tokens;
    }

    public void diagramLexedTokens(List<Token> _tokens)
    {
        for (Token token : _tokens)
        {
            System.out.println(token);
        }
    }

    public List<Token> parseSource(List<Token> _tokens)
    {
        if(_tokens.size() == 0)
        {
            throw new RuntimeException("No source code detected.");
        }

        List<Token> words = new ArrayList<>();

        Token rootToken = _tokens.get(0);
        Token scionToken = rootToken;

        if(rootToken.type != Token.TokenType.WORD_DEFINITION)
        {
            throw new RuntimeException("First token not a Word definition.");
        }

        for(int i = 1; i < _tokens.size(); i++)
        {
            Token currentToken = _tokens.get(i);

            if(currentToken.type != Token.TokenType.WORD_DEFINITION)
            {
                scionToken.nextToken = currentToken;
                scionToken = currentToken;
            } else
            {
                words.add(rootToken);
                rootToken = currentToken;
                scionToken = rootToken;
            }
        }

        words.add(rootToken);

        return words;
    }

    public void compileSource(List<Token> _tokenizedWordDefinitions)
    {
        for (Token token : _tokenizedWordDefinitions)
        {
            build(token);
        }
    }

    //#endregion

    //#region execution

    public Stack<Long> run(String _word, WordRuntime _runtime)
    {
        final Stack<Long> stack = new Stack<>();

        IWord word = bank.get(_word);

        if (word == null)
        {
            throw new RuntimeException("Unknown Word " + _word + ".");
        }

        _runtime.processWord(word, stack, 0, 0, 0, 0);

        _runtime.log("");

        _runtime.printStatistics();

        return stack;
    }

    public static Stack<Long> compileAndRun(String _source, FORTHLogger _logger, boolean _verbose)
    {
        if (_verbose)
        {
            _logger.log("SOURCE:");
            _logger.log(_source);
        }

        WordRuntime runtime = new WordRuntime(_logger, _verbose);

        WordCompiler compiler = generateCompiler(_logger, _verbose);

        List<Token> lexedTokens = compiler.lexSource(_source);

        List<Token> parsedTokens = compiler.parseSource(lexedTokens);

        if (_verbose)
        {
            _logger.log("\nTOKENS:\n");

            compiler.diagramLexedTokens(lexedTokens);
        }

        compiler.compileSource(parsedTokens);

        long start = System.nanoTime();

        if (_verbose)
        {
            runtime.log("\nCALLS:\n");
        }
        Stack<Long> stack = compiler.run("main", runtime);

        long end = System.nanoTime() - start;

        runtime.log("\nOUTPUTS:\n");

        int index = 0;
        for (long i : stack)
        {
            String bin = BigInteger.valueOf(i).toString(2);

            runtime.log(index + ": dec: " + i);// +  " bin: " + bin);
            index++;
        }

        runtime.log("\nPROGRAM RAN FOR " + end + " NS.\n");

        return stack;
    }
    //#endregion

    private static final Pattern WHITE_PATTERN = Pattern.compile("\\s|:|;|\\{|}|Ń");

    boolean isIllegalName(String _name)
    {
        boolean isNumber;
        try
        {
            Integer.parseInt(_name);
            isNumber = true;
        } catch (Exception e)
        {
            isNumber = false;
        }

        return (_name.length() == 0) || isNumber || WHITE_PATTERN.matcher(_name).find();
    }

    static final String SCOPE_OPEN = "{";
    static final String SCOPE_CLOSE = "}";
    static final String SCOPE_SUFFIX = "_ŚĆŌƤĔ_";

    static final String REPEAT = "repeat";

    static final String IF = "if";
}
