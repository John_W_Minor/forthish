package FORTH.logging;

@FunctionalInterface
public interface FORTHLogger
{
    void log(String _log);
}
