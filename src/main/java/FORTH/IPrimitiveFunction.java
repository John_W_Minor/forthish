package FORTH;

import java.util.Stack;

@FunctionalInterface
public interface IPrimitiveFunction
{
    IReturnCode process(Stack<Long> _stack, WordRuntime _runtime, long _eax, long _ebx, long _ecx, long _edx);
}
